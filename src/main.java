
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.sun.org.apache.xerces.internal.util.URI;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;

public class main {

  
    public static void main(String[] args) {
        URL url = null;
        
        
        try {
            url = new URL("https://eee.jotform.pro/interview/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            JsonParser parser = new JsonParser();

            String jsonString = content.toString();

            JsonElement jsonElement = parser.parse(jsonString);

            JsonObject jsonObjectt = jsonElement.getAsJsonObject();

            Gson gsonInstant = new Gson(); 
            HashMap<String, LinkedTreeMap> gson = gsonInstant.fromJson(jsonObjectt, HashMap.class);
            for (Entry<String, LinkedTreeMap> entry : gson.entrySet()) {
                LinkedTreeMap value = entry.getValue();
                System.out.println("Sitemizin ismi = " + value.get("name") + " /  " + "Sitemizin Url = " + value.get("url") + "\n");
               
            }

            in.close();

        } catch (URI.MalformedURIException e) {
            System.out.println("Client can not parse the URL correctly");
        } catch (ProtocolException e) {
            System.out.println("An Error protocol binding");
        } catch (IOException e) {
            System.out.println("Data is not collected");
        }

    }

}
